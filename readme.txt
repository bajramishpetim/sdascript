The script clones the source repository as a mirrored repository into a temporary directory.

It changes the remote URL of the mirrored repository to point to the GitLab repository.
It pushes the mirrored repository to the GitLab repository.

Finally, the script cleans up the temporary directory.


Team :) 
